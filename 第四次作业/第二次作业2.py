# 2、写一个生成大乐透号码的程序
#             生成随机号码：大乐透分前区号码和后区号码，
#             前区号码是从01-35中无重复地取5个号码，
#             后区号码是从01-12中无重复地取2个号码，组成一组7位的号码。


import random
l1=[i+1 for i in  range(35)]   #1-35的数组
l2=[i+1 for i in  range(12)]   #1-12的数组

def tool(n,*args):
    res = random.sample(args,n)  #在数组args中随机抽取n个数
    l = []
    i = 0
    while i<n:
        l.append(str(res[i]).zfill(2))   #补零
        i+=1
    l.sort()  #排序
    return l

def num(count):    #生成count个不重复的大乐透号码
    for i in range(count):
        letouhao = []
        s1 = ','.join(tool(5,*l1))
        s2 = ','.join(tool(2,*l2))
        s = s1+':'+s2
        if s not in letouhao:
            letouhao.append(s)
            print(s)
        else:
            i-=1

if __name__ == '__main__':
    count = int(input('请输入生成大乐透的个数：'))
    num(count)