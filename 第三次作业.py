# 字典操作+文件操作+函数
# 写一个商品管理的小程序
#          2、商品存在文件里面
#             1、添加商品
#                 输入产品名称、颜色、价格
#                 要校验商品是否存在，价格是否合法
#                 输入是否为空
#             2、修改商品信息
#                 输入产品名称、颜色、价格
#                 要校验商品是否存在，价格是否合法
#                 输入是否为空
#             3、查看商品信息
#                 输入商品名称
#                  输入all就展示全部商品
#                  输入的是其他的商品名称，
#             4、删除商品
#                 输入商品名称

import json
file_name = 'product.json'

#读取商品信息
def products():
    with open('product.json', encoding='utf-8') as f:
        str = json.load(f)#转换成字典
    return str

#检验商品名称
def check_name(name):
    if name in products():
        return True
    return False

#检查价格是否合理
def check_price(price):
    price = price.strip()
    if price and price.count('.') == 1:
        left, right = price.split('.')
        if left.isdigit() and right.isdigit() and int(right) < 100 and float(price) > 0:
            return True
        else:
            return False
    elif price.isdigit() and int(price) > 0:
        return True
    else:
        return False


#添加或修改商品信息
def updata_add(name,color,price):
    all_products = products()
    all_products[name] = {'color': color, 'price': price}
    with open('shangpin.txt', 'w', encoding='utf-8') as f:
        json.dump(all_products, f, ensure_ascii=False, indent=4)

#添加商品
def inset_products():
    name = (input('请输入商品名称：')).strip()
    color = (input('请输入颜色：')).strip()
    price = (input('请输入价格：')).strip()
    if name and color and price and check_name(name) and check_price(price):
        updata_add(name,color,price)
        print('添加成功！')
    elif name and check_name(name) is False:
        print('商品已存在！')
    else:
        print('输入错误！')

#修改商品
def updata_products():
    name = (input('请输入商品名称：')).strip()
    color = (input('请输入颜色：')).strip()
    price = (input('请输入价格：')).strip()
    if name and color and price and check_name(name) is False and check_price(price):
        updata_add(name,color,price)
        print('修改成功！')
    elif name and check_name(name):
        print('商品不存在！')
    else:
        print('输入错误')


#查看商品信息
def see_products():
   products_name = input('输入具体商品名称，要查看所有输入all：')
   if products_name.strip() != '':
        if products_name == 'all':
            print( products())
        elif products_name in  products():
            print(products_name, products()[ products_name])
        else:
            print('没有这个商品')
   else:
         print('输入错误！')

#删除商品
def del_products():
    products_name = (input('请输入商品名称：')).strip()
    if products_name.strip() != '':
        if products_name in products():
            all_goods = products()
            del all_goods[products_name]
            with open('shangpin.txt', 'w', encoding='utf-8') as f:
                json.dump(all_goods, f, ensure_ascii=False, indent=4)
                print('删除成功！')
        else:
            print('没有这个商品！')
    else:
        print('输入错误！')


while True:
    check = (input('请输入数字，1表示查询、2表示新增、3表示修改、4表示删除：')).strip()
    if check.isdigit():
        check = int(check)
        if check == 1:
            see_products()
        elif check == 2:
            inset_products()
            print(products())
        elif check ==3:
            updata_products()
            print(products())
        elif check == 4:
            del_products()
            print(products())
        else:
            print('输入错误请重新输入！')
    else:
        print('输入非数字！')