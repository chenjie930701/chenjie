# 第二个作业
#         1、写一个产生一批密码的程序，输入100，就产生100条密码
#         2、要求密码长度大于6，必须包含大写字母、小写字母、数字和特殊符号
#         3、每次产生的密码不能重复
#         4、写到文件里面
f = open('pwd.txt', encoding='utf-8')
result = f.read()  # 读取
f.close()

import random
import string
count=input('请输入数量：').strip()
count=int(count)
passwords=set()#定义一个空集合存放元素

for i in range(count):
    ran = string.digits + string.ascii_letters + string.punctuation  # 数字 + 大小写字母 + 特殊符号
    pwd = random.sample(string.ascii_uppercase, 1) + random.sample(string.punctuation, 1) + \
          random.sample(string.ascii_lowercase, 1) + random.sample(string.digits, 1)
    src = random.sample(ran, 5)  # 随机取5个元素
    pwds = pwd + src  # 两个list相加
    random.shuffle(pwds)  # 打乱顺序
    all_pwd = ''.join(pwds)  # 连接字符串
    passwords.add(all_pwd)
print(passwords)
for j in passwords:       #循环追加写入文件
    if passwords not in result:
        file = open('pwd.txt','a',encoding='utf-8')
        file.write(j+'\n')
        file.close()




